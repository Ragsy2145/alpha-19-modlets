
Required Legal Notice

Hind helicopter Modlet for 7 Days To Die Game.

All Models in this modlet are subject to creative commons licence.
https://creativecommons.org/licenses/by/4.0/

Model Credits : Ashley Aslett

No Models have been altered from the 'original creators' they are set up with appropriate lighting shader and textures in unity for 7 Days to Die game.

A big thanks to all the modellers who release work for free usage !

