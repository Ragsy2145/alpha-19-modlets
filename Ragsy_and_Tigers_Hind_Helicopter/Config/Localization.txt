Key,Source,Context,Changes,English,French,German,Klingon,Spanish,Polish

--------------------Items----------------------
vehicleHindSchematic,"[e7a729]Schematic: Helicopter[-]","[e7a729]Japanese[-]"
vehicleHindchassis,items,Part,EnChanged,Hind Chassis,,,,,
vehicleHindchassisDesc,items,Part,EnChanged,Parts needed to craft the Hind. Grease Monkey level 5 required.,,,,,
vehicleHindaccessories,items,Part,EnChanged,Hind Accessories,,,,,
vehicleHindaccessoriesDesc,items,Part,EnChanged,Parts needed to craft the Hind.,,,,,
vehicleHind,vehicles,vehicle,new,Hind,,,,,
vehicleHindplaceable,vehicles,item,EnChanged,Hind,,,,,
vehicleHindplaceableDesc,vehicles,item,EnChanged,Up Up and Away!!,,,,,