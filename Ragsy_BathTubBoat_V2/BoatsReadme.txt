Experimental work in progress Bathtub version 2.



Launch

Place the bathtub boat on/in water in a 'shallow water' area that has fairly flat ground underneath, if you have no shallow water in your map then build a small launch dock, then add fuel to bath tub boat and off you go. 

The Bath tub boat will behave irratically if placed on land is slow and steering compramised.


Controls
Steering is A and D as per vanilla setings ....mouse is for looking around only and has no affect on steering a boat.
Space bar and c control the up and down angles (same as a gyro) which you will need to adjust slightly as you travel on water otherwise you may sink.  
W is forwards and S is slow down/reverse ..If you let go of w and shift (Turbo) then the boat slowly comes to a stop, pressing s will slow the boat down
more quickly this replaces spacebar as the brake key.
To anchor the bath boat and stop it drifting away use c and spacebar to level the boat till it stops drifting, useful if you have a fishing mod installed.

Other Info
The bath tub boat can be picked up into inventory, if you sink it at any point, although be careful if you sink it too far away from the shores as it could be a long swim back. 
Best not store stuff as Zombies now can attack water vehicles in game.


Crafting
The bathtub boat is craftable in a standard workbench with no progression needed at the cost of resources by creating the required assemblies to build it,  it will help early game if you actually find a 'working workbench' and allow you to make the bath tub a lot sooner.

If anyone wants to change the recipes and build methods then please do so but let me know how you improved it.

Changelog
Version2
1. Optimised Model to reduce size of unity file.  
2. Added Particles ...these can be switched on or off with the F key ... (headlight function)
3. Depth mask applied to stop water seeping effect and allow it to ride lower in water
   for more realism effect.
4. Textures adjusted
5. Added working propeller



Happy Boating in A19

Ragsy!!

 

